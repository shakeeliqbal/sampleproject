﻿using BBP.EntityFramework.UnitOfWorks;
using BrainyBoilerplate.Data;
using BrainyBoilerplate.Data.Managers;
using BrainyBoilerplate.Service.Services.Students;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBP.DependencyResolver
{

    public class DependencyInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Kernel.ComponentRegistered += Kernel_ComponentRegistered;

            container.Register(Component.For<DbContext>().ImplementedBy<BrainyDbContext>().LifestylePerWebRequest());
            container.Register(Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestylePerWebRequest());
            
            container.Register(Component.For<IStudentManager>().LifeStyle.Transient.ImplementedBy<StudentManager>());
            container.Register(Component.For<IStudentService>().LifeStyle.Transient.ImplementedBy<StudentService>());

        }

        private void Kernel_ComponentRegistered(string key, Castle.MicroKernel.IHandler handler)
        {
            //throw new NotImplementedException();
        }

    }


}
