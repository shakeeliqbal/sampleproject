﻿using BBP.DependencyResolver;
using BrainyBoilerplate.Web.Dependency;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;

namespace BrainyBoilerplate.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {

        private WindsorContainer _windsorContainer;


        protected void Application_Start()
        {
            InitializeWindsor();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }



        protected void Application_End()
        {
            _windsorContainer?.Dispose();
        }


        private void InitializeWindsor()
        {
            _windsorContainer = new WindsorContainer();

            _windsorContainer.Install(FromAssembly.Containing<DependencyInstaller>());
            _windsorContainer.Install(FromAssembly.This());

            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(_windsorContainer.Kernel));
       

        }

    }
}
