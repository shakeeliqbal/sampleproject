﻿(function ($) {

    app.modals.CreateStudentModal = function () {


        var _modalManager;
        var _$StudentForm = null;

        function _findAssignedRoleNames() {

            var assignedRoleNames = [];

            return assignedRoleNames;

        }

        this.init = function (modalManager) {

            _modalManager = modalManager;

            _$StudentForm = _modalManager.getModal().find('form[name=StudentForm]');
            _$StudentForm.validate();

        };

        this.save = function () {


            if (!_$StudentForm.valid()) {
                return;
            }


            brainy.alert.confirm(
                                    "Are you sure?",
                                    function (isConfirmed) {
                                        if (isConfirmed) {

                                            alert("Call Method");

                                        } else {
                                            alert("Cancel");

                                        }
                                    }
                                );



            var formData = _$StudentForm.serializeObject();

            _modalManager.setBusy(true);

            commonAjaxManager.postAsync("/Admin/Student/CreateStudent", formData).done(function (data) {

                brainy.notify.info("Saved");
                _modalManager.close();

                brainy.event.trigger('brainy.createStudentModalSaved');

            }).always(function () {
                _modalManager.setBusy(false);
            });

            return false;

        };
    };
})(jQuery);