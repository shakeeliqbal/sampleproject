﻿var loadAllDataUrl;
var editPageUrl;
var _newStudentModel;
var _editStudentModel;

$(function () {

    loadAllDataUrl = app.rootUrl() + '/Admin/Student/LoadAllData';
    editPageUrl = app.rootUrl() + '/Admin/Student/Edit';

    var mainTable;

    _newStudentModel = new app.ModalManager({
        viewUrl: app.rootUrl() + '/Admin/Student/CreateModal',
        scriptUrl: app.rootUrl()  + '/Areas/Admin/Views/Student/_CreateModal.js',
        modalClass: 'CreateStudentModal'
    });

    _editStudentModel = new app.ModalManager({
        viewUrl: app.rootUrl() + '/Admin/Student/UpdateModal',
        scriptUrl: app.rootUrl() + '/Areas/Admin/Views/Student/_UpdateModal.js',
        modalClass: 'UpdateStudentModal'
    });

    $('#NewButton').click(function () {

        _newStudentModel.open();
    });

    var columns = [
                { "data": "Id", "name": "Id", "autoWidth": false },
                { "data": "Name", "name": "Name", "autoWidth": true },
                {
                    "data": null,
                    "orderable": false,
                    //"defaultContent": "<button><span class='fa fa-edit'></span></button>"
                    "render": function (data, type, full, meta) {

                        var span = $("<span></span>")
                        var deleteItem = $("<a id='deleteButton' href='javascript:openConfirmationDialog(\"Are you sure, you want to delete language?\",onDeleteCallback, " + full.Id + ");'><span class='fa fa-trash'></span></a> ");


                      //  $(span).append('<a href="javascript:openInterpreterModel(' + full.Id + ')"><i class="fa fa-bars"></i></a> ');

                        $(span).append('<a href="javascript:editStudent(' + full.Id + ')"><i class="fa fa-edit"></i></a> ');

                        $(span).append(deleteItem);

                        var html = span.html();

                        return html;

                    }
                }

    ];

    mainTable = commonManager.setUpDataTable("#myTable", loadAllDataUrl, columns);

    function onDeleteCallback(itemId) {

        commonAjaxManager.deleteRecordAsync('@Url.Action("Delete")', { Id: itemId }, onDeleteSuccess, onDeleteError);

    }

    function onDeleteSuccess(data) {

        hideConfirmationDialog();

        mainTable.ajax.reload();

    }

    function onDeleteError(data) {


    }

    function reloadTable() {
        mainTable.ajax.reload();
    }

    brainy.event.on('brainy.createStudentModalSaved', function () {
        reloadTable();
    });

    brainy.event.on('brainy.updateStudentModalSaved', function () {
        reloadTable();
    });

});

function editStudent(id) {
    _editStudentModel.open({ id: id });
}
