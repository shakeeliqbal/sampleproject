﻿using BBP.Common.JQueryDataTable;
using BBP.Common.JQueryDataTable.DataTable;
using BBP.Common.JQueryListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrainyBoilerplate.Web.Areas.Admin.Controllers
{
    public abstract class BaseAdminController : Controller
    {

        protected DTResult<T> GetListResult<T>(DTParameters param, ListResult<T> listResult)
        {

            DTResult<T> result = new DTResult<T>
            {
                draw = param.Draw,
                data = listResult.ResultData,
                recordsFiltered = listResult.TotalRecords,
                recordsTotal = listResult.TotalRecords
            };

            return result;

        }

        protected DTResult<T> GetListResult<T>(ListViewParameters param, ListResult<T> listResult)
        {

            DTResult<T> result = new DTResult<T>
            {
                draw = param.Draw,
                data = listResult.ResultData,
                recordsFiltered = listResult.TotalRecords,
                recordsTotal = listResult.TotalRecords
            };

            return result;

        }

    }

}