﻿using BBP.Common.JQueryDataTable;
using BBP.Common.JQueryDataTable.DataTable;
using BBP.Common.JQueryListView;
using BrainyBoilerplate.Models.Students;
using BrainyBoilerplate.Service.Services.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrainyBoilerplate.Web.Areas.Admin.Controllers
{
    public class StudentController : BaseAdminController
    {
        IStudentService _service;

        public StudentController(IStudentService service)
        {
            _service = service;
        }

        // GET: Admin/Student
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return View();
        }


        public PartialViewResult CreateModal()
        {

            var viewModel = new StudentCreateModel();

            return PartialView("_CreateModal", viewModel);

        }

        public PartialViewResult UpdateModal(StudentKeyModel model = null)
        {

            var viewModel = new StudentUpdateModel();

            if (model != null && model.Id > 0)
            {
                viewModel = _service.GetForEdit(model);
            }

            return PartialView("_UpdateModal", viewModel);

        }

        [HttpPost]
        public ActionResult LoadAllData(DTParameters param)
        {

            var listResult = _service.GetAll(param.GetSearchParameters());

            DTResult<StudentListModel> result = GetListResult<StudentListModel>(param, listResult);

            return Json(result);

        }

        [HttpPost]
        public ActionResult LoadAllListData(ListViewParameters param)
        {

            var listResult = _service.GetAll(param.GetSearchParameters());

            DTResult<StudentListModel> result = GetListResult<StudentListModel>(param, listResult);

            return Json(result);

        }

        [HttpPost]
        public bool CreateStudent(StudentCreateModel model)
        {
            _service.Create(model);

            return true;
        }

        [HttpPost]
        public bool UpdateStudent(StudentUpdateModel model)
        {
            _service.Update(model);

            return true;
        }

    }
}