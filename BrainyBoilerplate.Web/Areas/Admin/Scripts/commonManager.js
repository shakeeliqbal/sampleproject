﻿var commonManager = new function () {
    var _this = this;

    

    this.setUpDataTable = function (selector, url, columns, defaultOrder, fillExtraData) {


        var main_table_data = $(selector).DataTable({
            "order": defaultOrder,
            "processing": true, // for show progress bar
            "serverSide": true, // for process server side
            "filter": true, // this is for disable filter (search box)
            // "searchDelay": 1000,
            "orderMulti": false, // for disable multiple column at once
            "ajax": {
                "url": url,
                "type": "POST",
                "datatype": "json",
                "data": function (d) {
                    if (fillExtraData) {
                        fillExtraData(d);
                    }


                }

            },
            "columns": columns
        });


        $(selector + '_filter input').unbind();
        $(selector + '_filter input').bind('keyup', function (e) {


            var enterVal = this.value;

            if (e.keyCode == 13) {
                main_table_data.search(enterVal).draw();
            }
            else if (e.keyCode == 8 || e.keyCode == 46) {
                if (!enterVal || enterVal.length == 0) {
                    main_table_data.search("").draw();
                }
            }


        });


        return main_table_data;
    };



    this.setUpDataTableNoPaging = function (selector, url, columns, defaultOrder, fillExtraData) {


        var table = $(selector).DataTable({
            "order": defaultOrder,
            "pageLength": -1,
            "bPaginate": false,
            "bLengthChange": false,
            "processing": true, // for show progress bar
            "serverSide": true, // for process server side
            "filter": true, // this is for disable filter (search box)
            // "searchDelay": 1000,
            "orderMulti": false, // for disable multiple column at once
            "ajax": {
                "url": url,
                "type": "POST",
                "datatype": "json",
                "data": function (d) {
                    if (fillExtraData) {
                        fillExtraData(d);
                    }


                }

            },
            "columns": columns
        });



        return table;
    };




    this.showSuccessMessage = function (message) {

        if (!message) {
            message = "Saved Successfully.";
        }

        _this.showMessage(message, 'alert-success');

    };


    this.showErrorMessage = function (message) {

        if (!message) {
            message = "Error.";
        }

        _this.showMessage(message, 'alert-danger');

    };

    this.showMessage = function (message, bootstrapCss) {
        if (!message) {
            message = "No Message.";
        }

        var alertHtml = '<div class="alert ' + bootstrapCss + ' tolk-alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>';
        $(alertHtml).appendTo('body').delay(3000).fadeOut("slow", function () { $(alertHtml).remove() });
    };


};


