﻿$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};




$.fn.buttonBusy = function (isBusy) {
    return $(this).each(function () {
        var $button = $(this);
        var $icon = $button.find('i');
        var $buttonInnerSpan = $button.find('span');

        if (isBusy) {
            if ($button.hasClass('button-busy')) {
                return;
            }

            $button.attr('disabled', 'disabled');

            //change icon
            if ($icon.length) {
                $button.data('iconOriginalClasses', $icon.attr('class'));
                $icon.removeClass();
                $icon.addClass('fa fa-spin fa-spinner');
            }

            //change text
            if ($buttonInnerSpan.length && $button.attr('busy-text')) {
                $button.data('buttonOriginalText', $buttonInnerSpan.html());
                $buttonInnerSpan.html($button.attr('busy-text'));
            }

            $button.addClass('button-busy');
        } else {
            if (!$button.hasClass('button-busy')) {
                return;
            }

            //enable button
            $button.removeAttr('disabled');

            //restore icon
            if ($icon.length && $button.data('iconOriginalClasses')) {
                $icon.removeClass();
                $icon.addClass($button.data('iconOriginalClasses'));
            }

            //restore text
            if ($buttonInnerSpan.length && $button.data('buttonOriginalText')) {
                $buttonInnerSpan.html($button.data('buttonOriginalText'));
            }

            $button.removeClass('button-busy');
        }
    });
};
