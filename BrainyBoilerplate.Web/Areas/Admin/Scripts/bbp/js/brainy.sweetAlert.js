﻿var brainy = brainy || {};
(function ($) {
	if (!sweetAlert || !$) {
		return;
	}

	/* DEFAULTS *************************************************/

	brainy.libs = brainy.libs || {};
	brainy.libs.sweetAlert = {
		config: {
			'default': {

			},
			info: {
				type: 'info'
			},
			success: {
				type: 'success'
			},
			warn: {
				type: 'warning'
			},
			error: {
				type: 'error'
			},
			confirm: {
				type: 'warning',
				title: 'Are you sure?',
				showCancelButton: true,
				cancelButtonText: 'Cancel',
				confirmButtonColor: "#DD6B55",
				confirmButtonText: 'Yes'
			}
		}
	};

	/* MESSAGE **************************************************/

	var showAlert = function (type, message, title) {
		if (!title) {
			title = message;
			message = undefined;
		}

		var opts = $.extend(
            {},
            brainy.libs.sweetAlert.config.default,
            brainy.libs.sweetAlert.config[type],
            {
            	title: title,
            	text: message
            }
        );

		return $.Deferred(function ($dfd) {
			sweetAlert(opts, function () {
				$dfd.resolve();
			});
		});
	};

	brainy.alert.info = function (message, title) {
		return showAlert('info', message, title);
	};

	brainy.alert.success = function (message, title) {
		return showAlert('success', message, title);
	};

	brainy.alert.warn = function (message, title) {
		return showAlert('warn', message, title);
	};

	brainy.alert.error = function (message, title) {
		return showAlert('error', message, title);
	};

	brainy.alert.confirm = function (message, titleOrCallback, callback) {
		var userOpts = {
			text: message
		};

		if ($.isFunction(titleOrCallback)) {
			callback = titleOrCallback;
		} else if (titleOrCallback) {
			userOpts.title = titleOrCallback;
		};

		var opts = $.extend(
            {},
            brainy.libs.sweetAlert.config.default,
            brainy.libs.sweetAlert.config.confirm,
            userOpts
        );

		return $.Deferred(function ($dfd) {
			sweetAlert(opts, function (isConfirmed) {
				callback && callback(isConfirmed);
				$dfd.resolve(isConfirmed);
			});
		});
	};
    
})(jQuery);