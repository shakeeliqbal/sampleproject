﻿var brainy = brainy || {};
(function () {

    if (!toastr) {
        return;
    }

    /*********************************************************************
    ****************** SET DEFAULT TOASTR LOCATION ***********************
    **********************************************************************/

    toastr.options.positionClass = 'toast-bottom-right';

    /*********************************************************************
    *********************** METHODS FOR TOASTR ***************************
    **********************************************************************/

    brainy.notify.success = function (message, title, options) {
        showToastrMessage('success', message, title, options);
    };

    brainy.notify.info = function (message, title, options) {
        showToastrMessage('info', message, title, options);
    };

    brainy.notify.warn = function (message, title, options) {
        showToastrMessage('warning', message, title, options);
    };

    brainy.notify.error = function (message, title, options) {
        showToastrMessage('error', message, title, options);
    };

    // SHOW MESSAGE COMMON METHOD
    var showToastrMessage = function (type, message, title, options) {
        toastr[type](message, title, options);
    };

})();