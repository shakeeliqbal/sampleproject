﻿var brainy = brainy || {};

(function ($) {





    /* NOTIFY **********************************************/

    brainy.notify = brainy.notify || {};

    brainy.notify.success = function (message, title, options) {
        alert(message);
    };

    brainy.notify.info = function (message, title, options) {
        alert(message);
    };

    brainy.notify.warn = function (message, title, options) {
        alert(message);
    };

    brainy.notify.error = function (message, title, options) {
        alert(message);
    };




    /* ALERTS ***************************************************************/
    //Defines Alert API, not implements it

    brainy.alert = brainy.alert || {};

    var showAlert = function (message, title) {
        alert((title || '') + ' ' + message);

    };

    brainy.alert.info = function (message, title) {
        return showAlert(message, title);
    };

    brainy.alert.success = function (message, title) {
        return showAlert(message, title);
    };

    brainy.alert.warn = function (message, title) {
        return showAlert(message, title);
    };

    brainy.alert.error = function (message, title) {
        return showAlert(message, title);
    };

    brainy.alert.confirm = function (message, titleOrCallback, callback) {

        if (titleOrCallback && !(typeof titleOrCallback == 'string')) {
            callback = titleOrCallback;
        }

        var result = confirm(message);
        callback && callback(result);

        if (!$) {
            return null;
        }

        return $.Deferred(function ($dfd) {
            $dfd.resolve();
        });
    };




    /* SIMPLE EVENT BUS *****************************************/

    brainy.event = (function () {

        var _callbacks = {};

        var on = function (eventName, callback) {
            if (!_callbacks[eventName]) {
                _callbacks[eventName] = [];
            }

            _callbacks[eventName].push(callback);
        };

        var off = function (eventName, callback) {
            var callbacks = _callbacks[eventName];
            if (!callbacks) {
                return;
            }

            var index = -1;
            for (var i = 0; i < callbacks.length; i++) {
                if (callbacks[i] === callback) {
                    index = i;
                    break;
                }
            }

            if (index < 0) {
                return;
            }

            _callbacks[eventName].splice(index, 1);
        };

        var trigger = function (eventName) {
            var callbacks = _callbacks[eventName];
            if (!callbacks || !callbacks.length) {
                return;
            }

            var args = Array.prototype.slice.call(arguments, 1);
            for (var i = 0; i < callbacks.length; i++) {
                callbacks[i].apply(this, args);
            }
        };

        // Public interface ///////////////////////////////////////////////////

        return {
            on: on,
            off: off,
            trigger: trigger
        };
    })();


})(jQuery);