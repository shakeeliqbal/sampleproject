﻿(function ($) {

    $.fn.listview = function (options) {

        var _container = null;

        var mainDataModel = {
            Draw: 1,
            Start: 0,
            Length: -1,
            SortOrderColumnName: '',
            SortOrderDirection:0

        };

        var settings = $.extend({
            submitButton: false,
            toggleButton: false,
            disable_copy: true,
            strength: false,
            strengthBar: false,
            secondaryInput: false,
            passwordMinLength: 8,
            loading: $('<div id="brainy_listview_Loading_wrapper"><span id="brainy_listview_Loading_content">Loading...</span></div>')
        }, options);

        _container = $(this);

        //$(_container).addClass("progressbar")
        //   .text(settings.value);
      

        loadData();




        function loadData()
        {

            _showLoader();
         
            $.ajax({
                url: settings.ajax.url,
                type: 'Post',
                data: mainDataModel,
                // dataType: "json",
                // contentType: "application/json",
                cache: false,
                success: function (data) {
                    _hideLoading();
                    if(data)
                    {
                        for(var i=0;i<data.data.length;i++)
                        {
                            var html = settings.render(data.data[i]);

                            _container.append(html);
                        }
                    }



                },
                error: function (xhr) {
                    _hideLoading()
                    alert("Error..");
                  

                },
                completed: function () {
                    alert("Finished");
                }


            });
        }


         var _loader = "dssdsd";

        function _showLoader()
        {
            //alert("load")
            _container.append(settings.loading);
        }

        function _hideLoading() {
            $(settings.loading).remove();
        }


       

    };

}(jQuery));