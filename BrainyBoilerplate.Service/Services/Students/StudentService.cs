﻿using BrainyBoilerplate.Models.Students;
using BrainyBoilerplate.Service.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrainyBoilerplate.Data.Managers;
using BrainyBoilerplate.Data.Entities;
using BrainyBoilerplate.Service.Mapping.Base;
using BrainyBoilerplate.Service.Mapping.Students;
using BBP.Common.JQueryDataTable;
using BBP.Common.Extensions;
using BrainyBoilerplate.Data;
using BBP.EntityFramework.UnitOfWorks;

namespace BrainyBoilerplate.Service.Services.Students
{
    public class StudentService : IStudentService
    {

        IStudentManager _defaultManager;
        IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork, IStudentManager defaultManager)
        {
            _unitOfWork = unitOfWork;
            _defaultManager = defaultManager;
        }

        public IStudentManager CurrentManager
        {
            get
            {
                return _defaultManager;
            }
        }

        IMapping<StudentModel, Student> _mapper;
        public IMapping<StudentModel, Student> Mapper
        {
            get
            {
                return _mapper ?? (_mapper = new StudentMapping());
            }
        }

        public StudentKeyModel Create(StudentCreateModel model)
        {
            var dbItem = new Student();

            dbItem.Name = model.Name;
            dbItem.Address = model.Address;
            dbItem.PhoneNumber = model.PhoneNumber;

            dbItem.DateCreated = DateTime.Now;
            CurrentManager.Insert(dbItem);

            _unitOfWork.SaveChanges();

            return new StudentKeyModel(dbItem.Id);
        }

        public void Update(StudentUpdateModel model)
        {
            var dbItem = CurrentManager.Find(model.Id);

            dbItem.Name = model.Name;
            dbItem.Address = model.Address;
            dbItem.PhoneNumber = model.PhoneNumber;

            _unitOfWork.SaveChanges();

        }

        public bool Delete(StudentKeyModel model)
        {
            CurrentManager.Delete(model.Id);
            _unitOfWork.SaveChanges();

            return true;
        }

        public StudentModel Get(StudentKeyModel model)
        {
            var item = CurrentManager.Find(model.Id);

            return Mapper.ConvertToModel(item);
        }

        public StudentUpdateModel GetForEdit(StudentKeyModel model)
        {
            var item = CurrentManager.Find(model.Id);

            return new StudentUpdateModel
            {
                Id = item.Id,
                Name = item.Name,
                PhoneNumber = item.PhoneNumber,
                Address = item.Address
            };
        }

        public List<StudentModel> GetAll()
        {
            var items = CurrentManager.All;
            return Mapper.ConvertToModel(items).ToList();
        }

        public ListResult<StudentListModel> GetAll(SearchParameters searchParameters)
        {
            try
            {
                if (searchParameters == null)
                {
                    throw new NullReferenceException("Search Parameters Cannot be null");
                }

                var items = CurrentManager.All.Select(x => new StudentListModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    DateCreated = x.DateCreated
                });

                // Filter records using method implemented in drive class.
                if (!string.IsNullOrEmpty(searchParameters.SearchText))
                {
                    items = items.Where(x =>
                                            x.Name.Contains(searchParameters.SearchText)
                                       );
                }


                // Get total count of Filtered Records
                var totalRecords = items.Count();

                // Apply Sort Order
                items = items.ApplySortingAndPagging(searchParameters.SortColumnName, searchParameters.SortOrderDescending, searchParameters.PageSize, searchParameters.PageStart);

                // Return Result
                var returnObject = new ListResult<StudentListModel>();

                returnObject.TotalRecords = totalRecords;
                returnObject.ResultData = items.ToList();

                return returnObject;
            }
            catch (Exception ex)
            {
                var msg = ex.ToString();
                throw;
            }
        }

        private IQueryable<StudentListModel> FilterRecords(IQueryable<StudentListModel> items, SearchParameters searchParameters)
        {

            if (!string.IsNullOrEmpty(searchParameters.SearchText))
            {
                items = items.Where(x =>
                                        x.Name.Contains(searchParameters.SearchText)
                                   );
            }

            return items;
        }


    }

}
