﻿using BBP.Common.JQueryDataTable;
using BrainyBoilerplate.Models.Students;
using BrainyBoilerplate.Service.Services.Base;


namespace BrainyBoilerplate.Service.Services.Students
{
    public interface IStudentService : IBaseService<StudentModel, StudentKeyModel, StudentCreateModel, StudentUpdateModel, StudentListModel>
    {

    }
}
