﻿using BBP.Common.JQueryDataTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Service.Services.Base
{
    public interface IBaseService<TModel, PK, TCreateModel, TUpdateModel, TListModel>
    {

        TModel Get(PK model);

        TUpdateModel GetForEdit(PK model);

        PK Create(TCreateModel model);

        void Update(TUpdateModel model);

        bool Delete(PK id);
        
        ListResult<TListModel> GetAll(SearchParameters searchParameters);

        List<TModel> GetAll();

    }

}
