﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Service.Mapping.Base
{
    public interface IMapping<Tmodel, TdbModel>
    {

        Tmodel ConvertToModel(TdbModel dataItem);

        TdbModel ConvertToDbModel(Tmodel dataItem);

        void ConvertToDbModel(Tmodel dataItem, TdbModel item);

        IEnumerable<Tmodel> ConvertToModel(IEnumerable<TdbModel> dataItems);

    }
}
