﻿using BrainyBoilerplate.Data.Entities;
using BrainyBoilerplate.Models.Students;
using BrainyBoilerplate.Service.Mapping.Base;
using System.Collections.Generic;
using System.Linq;

namespace BrainyBoilerplate.Service.Mapping.Students
{
    public class StudentMapping : IMapping<StudentModel, Student>
    {

        public StudentModel ConvertToModel(Student item)
        {
            return new StudentModel
            {
                Id = item.Id,
                Name = item.Name,
                Address = item.Address,
                PhoneNumber = item.PhoneNumber,
                DateCreated = item.DateCreated
            };
        }

        public Student ConvertToDbModel(StudentModel item)
        {
            var dbItem = new Student();

            ConvertToDbModel(item, dbItem);


            return dbItem;
        }

        public void ConvertToDbModel(StudentModel item, Student dbItem)
        {
            dbItem.Id = item.Id;
            dbItem.Name = item.Name;
            dbItem.Address = item.Address;
            dbItem.PhoneNumber = item.PhoneNumber;
        }

        public IEnumerable<StudentModel> ConvertToModel(IEnumerable<Student> item)
        {

            return item.Select(x => ConvertToModel(x));

        }

    }
}
