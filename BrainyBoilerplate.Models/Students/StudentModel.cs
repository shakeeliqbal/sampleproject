﻿using BrainyBoilerplate.Models.Base;
using System;


namespace BrainyBoilerplate.Models.Students
{
    public class StudentModel : BaseModel
    {

        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateCreated { get; set; }
        
    }

}
