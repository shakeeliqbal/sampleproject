﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Models.Students
{
    public class StudentKeyModel
    {

        public StudentKeyModel() { }
        
        public StudentKeyModel(long id)
        {
            Id = id;
        }
        
        public long Id { get; set; }

    }
}
