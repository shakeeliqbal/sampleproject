﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace BrainyBoilerplate.Data.Entities
{
    [Table("Student")]
    public class Student
    {      

        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateCreated { get; set; }

    }

}
