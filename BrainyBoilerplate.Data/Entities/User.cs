﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Data.Entities
{
    [Table("User")]
    public class User
    {
        public User()
        {

        }

        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }
        
        public string Password { get; set; }
        
        public DateTime DateCreated { get; set; }

    }

}
