﻿using BBP.EntityFramework.BaseManagers;
using BrainyBoilerplate.Data.Entities;
using System.Data.Entity;

namespace BrainyBoilerplate.Data.Managers
{
    public class StudentManager : BaseManager<Student>, IStudentManager
    {
        public StudentManager(DbContext dbEntities):base(dbEntities)
        {
          
        }
    }
}
