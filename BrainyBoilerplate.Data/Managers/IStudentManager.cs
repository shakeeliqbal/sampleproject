﻿using BBP.EntityFramework.BaseManagers;
using BrainyBoilerplate.Data.Entities;

namespace BrainyBoilerplate.Data.Managers
{
   public interface IStudentManager : IManager<Student>
    {
    }
}
