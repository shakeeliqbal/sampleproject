﻿using BrainyBoilerplate.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Data
{

    public class BrainyDbContext : DbContext
    {
        public BrainyDbContext() : base("Default")
        {

        }

        public IDbSet<Student> Students { get; set; }

        public IDbSet<User> Users { get; set; }
        

    }
}
