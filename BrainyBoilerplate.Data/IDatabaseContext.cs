﻿using BrainyBoilerplate.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainyBoilerplate.Data
{
    public interface IDatabaseContext
    {
         IDbSet<Student> Students { get; set; }

         IDbSet<User> Users { get; set; }

        DbSet<T> Set<T>() where T : class;

        void Dispose();
    }
}
